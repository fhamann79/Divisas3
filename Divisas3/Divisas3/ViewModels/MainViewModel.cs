﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Divisas3.ViewModels
{
    public class MainViewModel : BindableObject
    {

        private Decimal dollars;
        private Decimal euros;
        private Decimal pesos;
        private Decimal pounds;

        #region Properties
        public Decimal Dollars
        {
            get
            {
                return dollars;
            }
            set
            {
                if (dollars!=value)
                {
                    dollars = value;

                    OnPropertyChanged("Dollars");
                }
            }
        }

        public Decimal Pesos
        {
            get
            {
                return pesos;
            }
            set
            {
                if (pesos != value)
                {
                    pesos = value;

                    OnPropertyChanged("Pesos");
                }
            }
        }

        public Decimal Pounds
        {
            get
            {
                return pounds;
            }
            set
            {
                if (pounds != value)
                {
                    pounds = value;

                    OnPropertyChanged("Pounds");
                }
            }
        }

        public Decimal Euros
        {
            get
            {
                return euros;
            }
            set
            {
                if (euros != value)
                {
                    euros = value;

                    OnPropertyChanged("Euros");
                }
            }
        }

        #endregion



        public ICommand ConvertCommand { get { return new Command(ConvertMoney); } }

        private void ConvertMoney()
        {
            if (Dollars <= 0)
            {
                return;
            }

            Pesos = Dollars * (decimal)3039.51368;
            Pounds = Dollars * (decimal)0.789973654;
            Euros = Dollars * (decimal)0.897070169;
            


        }
    }
}
